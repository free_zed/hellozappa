#!/usr/bin/env python3
#-*- coding:utf-8 -*-
"""
Author: fred <git@freezed.me> 2019-02-22
Licence: `GNU GPL v3` GNU GPL v3: http://www.gnu.org/licenses/

A basic HTML/CSS templated response

This file is part of [hellozappa](https://gitlab.com/free_zed/hellozappa/)
"""
from random import randrange as rr
from flask import Flask, render_template
from utils.staticurl import get_local, get_prod
from utils.gcd import get_gcd


app = Flask(__name__)
app.config.from_object('config')
app.add_template_global(get_prod, 'get_static_url')


@app.route('/', defaults={'int_a': None, 'int_b': None})
@app.route('/<int:int_a>/<int:int_b>')
def index(int_a, int_b):
    """ HTML/CSS templated response of a calculation function """

    template = "stage4.html"
    response = {
        'int_a' : rr(4, 1000, 2),
        'int_b' : rr(4, 1000, 2),
    }

    if int_a is not None and int_b is not None:
        template = "response4.html"
        response = {
            'result' : get_gcd(int_a, int_b),
            'int_a' : int_a,
            'int_b' : int_b,
        }

    return render_template(template, **response)


# for local development
if __name__ == '__main__':
    app.config.from_object('config')
    app.config['STAGE'][4]['URL'] = 'http://127.0.0.1:5004'
    app.add_template_global(get_local, 'get_static_url')
    app.debug = True
    app.run(port=5004)
