#!/usr/bin/env python3
#-*- coding:utf-8 -*-
"""
Author: fred <git@freezed.me> 2019-02-21
Licence: `GNU GPL v3` GNU GPL v3: http://www.gnu.org/licenses/

Utilities for [hellozappa](https://gitlab.com/free_zed/hellozappa/)
"""
from urllib.parse import urljoin
from flask import Flask, url_for


app = Flask(__name__)
app.config.from_object('config')


def get_local(filename):
    """
    Normal static URL generation : for local development
    """
    return url_for('static', filename=filename)


def get_prod(filename):
    """
    Set static URL to PROD_STATIC_URL : for production
    See : https://stackoverflow.com/a/35824550/6709630
    """
    prod_static_url = app.config.get('PROD_STATIC_URL')
    return urljoin(prod_static_url, filename)
