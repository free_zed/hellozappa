#!/usr/bin/env python3
#-*- coding:utf-8 -*-
"""
Author: fred <git@freezed.me> 2019-02-21
Licence: `GNU GPL v3` GNU GPL v3: http://www.gnu.org/licenses/

Utilities for [hellozappa](https://gitlab.com/free_zed/hellozappa/)
"""

def get_gcd(int_a, int_b):
    """
    Find the greater commun divisor of two integers

    :int_a: an positiv integer
    :int_b: an other positiv integer

    :Tests:
    >>> get_gcd(63, 42)
    21
    >>> get_gcd(357, 561)
    51
    >>> get_gcd(21, 15)
    3
    >>> get_gcd(910, 42)
    14
    """

    rest_list = []

    #Initial soustraction
    nmax = max(int_a, int_b)
    nmin = min(int_a, int_b)
    rest = nmax - nmin

    # Reduction
    while rest != 0:
        nmax = max(nmin, rest)
        nmin = min(nmin, rest)
        rest_list.append(rest)

        rest = nmax - nmin

    rest_list.reverse()
    return rest_list[0]


if __name__ == "__main__":
    import doctest
    doctest.testmod()
