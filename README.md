Hello Zappa!
===========

Python & Flask & Zappa & Lambda
-------------------------------

A sandbox to play with Python & server-less applications.

Built with [Flask][flask] & deployed on [AWS Lambda][aws-lbd] with [Zappa][zappa].

### Roadmap

- [x] Run a basic application : [see code][stage1-tag] & [click to see on AWS][stage1]
- [x] Add a `/json/` route to get JSON response : [see code][stage2-tag] & [click to see on AWS][stage2]
- [x] Render with an HTML/CSS template : [see code][stage3-tag] & [click to see on AWS][stage3]
- [x] Automated deployment with Gitlab-CI
- [x] Add a function running some calculation : [see code][stage4-tag] & [click to see on AWS][stage4]
- [ ] Add an HTML form to send data to function


### Misc

- Last version of this doc on [Gitlab][repo]
- More stuff from me [just here][freezed]


[aws-lbd]: https://console.aws.amazon.com/lambda/ "AWS Lambda lets you run code without provisioning or managing servers"
[flask]: http://flask.pocoo.org "Flask : web developpement one drop at a time"
[freezed]: http://pro.zind.fr "More stuff from freezed"
[repo]: http://gitlab.com/free_zed/hellozappa/
[stage1]: https://j0h7ilvb4m.execute-api.eu-west-3.amazonaws.com/stage1
[stage1-tag]: http://gitlab.com/free_zed/hellozappa/tags/stage1/
[stage2]: https://rg5i2t5580.execute-api.eu-west-3.amazonaws.com/stage2
[stage2-tag]: http://gitlab.com/free_zed/hellozappa/tags/stage2/
[stage3]: https://74qod2yz1c.execute-api.eu-west-3.amazonaws.com/stage3
[stage3-tag]: http://gitlab.com/free_zed/hellozappa/tags/stage3/
[stage4]: https://648mi3vtw6.execute-api.eu-west-3.amazonaws.com/stage4
[stage4-tag]: http://gitlab.com/free_zed/hellozappa/tags/stage4/
[zappa]: https://github.com/Miserlou/Zappa/#zappa---serverless-python "Build and deploy server-less, event-driven Python applications"
