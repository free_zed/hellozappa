#!/usr/bin/env python3
#-*- coding:utf-8 -*-
"""
Author: fred <git@freezed.me> 2019-02-21
Licence: `GNU GPL v3` GNU GPL v3: http://www.gnu.org/licenses/

A basic HTML/CSS templated response

This file is part of [hellozappa](https://gitlab.com/free_zed/hellozappa/)
"""

from flask import Flask, render_template
from utils.staticurl import get_local, get_prod

app = Flask(__name__)
app.config.from_object('config')
app.add_template_global(get_prod, 'get_static_url')


@app.route('/', defaults={'url_param': 'w0rld'})
@app.route('/<string:url_param>')
def index(url_param):
    """ HTML/CSS templated response """

    context = {"r_origin": url_param, "r_upper": url_param.upper()}
    return render_template("stage3.html", **context)


# for local development
if __name__ == '__main__':
    app.config.from_object('config')
    app.config['STAGE'][3]['URL'] = 'http://127.0.0.1:5003'
    app.add_template_global(get_local, 'get_static_url')
    app.debug = True
    app.run(port=5003)
