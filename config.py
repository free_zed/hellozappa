#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Author: freezed <git@freezed.me> 2019-02-11
Licence: `GNU GPL v3` GNU GPL v3: http://www.gnu.org/licenses/

This file is part of [hellozappa](https://gitlab.com/free_zed/hellozappa/)
"""
GPL_URL = "https://www.gnu.org/licenses/gpl-3.0.fr.html"
AUTHOR_NAME = "freezed"
AUTHOR_URL = "http://pro.zind.fr"
PROD_STATIC_URL = "https://free_zed.gitlab.io/hellozappa/"

APP = {
    'NAME': 'Hello Zappa',
    'BASELINE': 'Python & Flask & Zappa & Lambda',
    'SRC': 'https://gitlab.com/free_zed/hellozappa/',
}

# Stages related
STAGE = {
    1: {
        "LABEL": "stage1",
        "URL": "https://j0h7ilvb4m.execute-api.eu-west-3.amazonaws.com/stage1",
    },
    2: {
        "LABEL": "stage2",
        "URL": "https://rg5i2t5580.execute-api.eu-west-3.amazonaws.com/stage2",
    },
    3: {
        "LABEL": "stage3",
        "URL": "https://6gxvkfin9l.execute-api.eu-west-3.amazonaws.com/stage3",
    },
    4: {
        "LABEL": "stage4",
        "URL": "https://648mi3vtw6.execute-api.eu-west-3.amazonaws.com/stage4",
    },
}
